package ru.tsc.tambovtsev.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.tambovtsev.tm.api.model.IWBS;
import ru.tsc.tambovtsev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractWbsModel extends AbstractModel implements IWBS {

    @NotNull
    protected static final String FORMAT_DATE_TIME = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    @NotNull
    protected static final String FORMAT_DATE_UI = "yyyy-MM-dd";

    @NotNull
    @Column
    private String name = "";

    @Column
    @Nullable
    private String description = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "date_begin")
    @DateTimeFormat(pattern = FORMAT_DATE_UI)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMAT_DATE_TIME)
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @DateTimeFormat(pattern = FORMAT_DATE_UI)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMAT_DATE_TIME)
    private Date dateEnd;

    public AbstractWbsModel(
            @NotNull final String name
    ) {
        super();
        this.name = name;
    }

    public AbstractWbsModel(
            @NotNull final String name,
            @Nullable final String description
    ) {
        super();
        this.name = name;
        this.description = description;
    }

    public AbstractWbsModel(
            @NotNull final String name,
            @NotNull final Status status,
            @Nullable final Date dateBegin
    ) {
        super();
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public AbstractWbsModel(
            @NotNull final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        super();
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    @Override
    public String toString() {
        return name + " : " + getStatus().getDisplayName() + " : " + description;
    }

}
