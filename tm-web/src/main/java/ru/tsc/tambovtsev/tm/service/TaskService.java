package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public final class TaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    public Task create() {
        @NotNull final Task task = new Task(
                "New Task: " + System.currentTimeMillis(),
                "Description",
                new Date(),
                new Date());
        save(task);
        return task;
    }

    public Task save(@NotNull final Task task) {
        return repository.save(task);
    }

    @Nullable
    public Collection<Task> findAll() {
        return repository.findAll();
    }

    @Nullable
    public Task findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    public void removeById(@NotNull final String id) {
        repository.deleteById(id);
    }

    public void remove(@NotNull final Task task) {
        repository.delete(task);
    }

    public void remove(@NotNull final List<Task> tasks) {
        tasks
                .stream()
                .forEach(this::remove);
    }

    public boolean existsById(@NotNull final String id) {
        return repository.existsById(id);
    }

    public void clear() {
        repository.deleteAll();
    }

    public long count() {
        return repository.count();
    }
    
}
