package ru.tsc.tambovtsev.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.tsc.tambovtsev.tm.api.endpoint.ProjectRestEndpoint;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.Arrays;
import java.util.List;

public class ProjectRestEndpointClient implements ProjectRestEndpoint {

    @NotNull
    private static final String ROOT_URL = "http://localhost:8080/api/project/";

    @Override
    public List<Project> findAll() {
        @NotNull final String localUrl = "findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        return Arrays.asList(template.getForObject(ROOT_URL + localUrl, Project[].class));
    }

    @Override
    public Project findById(@NotNull final String id) {
        @NotNull final String localUrl = "findById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Project.class, id);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        @NotNull final String localUrl = "existsById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Boolean.class, id);
    }

    @Override
    public Project save(@NotNull final Project project) {
        @NotNull final String localUrl = "save";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(project, headers);
        return template.postForObject(ROOT_URL + localUrl, entity, Project.class);
    }

    @Override
    public void delete(@NotNull final Project project) {
        @NotNull final String localUrl = "delete";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(project, headers);
        template.postForObject(ROOT_URL + localUrl, entity, Project.class);
    }

    @Override
    public void clear(@NotNull final List<Project> project) {
        @NotNull final String localUrl = "deleteAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(project, headers);
        template.postForObject(ROOT_URL + localUrl, entity, Project.class);
    }

    @Override
    public void clear() {
        @NotNull final String localUrl = "clear";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(ROOT_URL + localUrl);
    }

    @Override
    public void deleteById(@NotNull final String id) {
        @NotNull final String localUrl = "deleteById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(ROOT_URL + localUrl, id);
    }

    @Override
    public long count() {
        @NotNull final String localUrl = "count";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Long.class);
    }

    public static void main(String[] args) {
        ProjectRestEndpointClient client = new ProjectRestEndpointClient();

        @NotNull final List<Project> projects = client.findAll();
        System.out.println("List projects:");
        for(@NotNull final Project project: projects) {
            System.out.printf("Project: %s; ID: %s%n", project.getName(), project.getId());
        }
        System.out.println();

        @NotNull final String id = projects.get(0).getId();

        System.out.printf("Count projects: %d%n", client.count());
        System.out.printf("Exists project: %b%n", client.existsById(id));

        @Nullable final Project project = client.findById(id);
        System.out.printf("Find project: %s%n", project.getName());

        client.deleteById(id);
        System.out.printf("Count projects: %d%n", client.count());
        System.out.printf("Exists project: %b%n", client.existsById(id));

        client.save(project);
        System.out.printf("Count projects: %d%n", client.count());

        client.delete(project);
        System.out.printf("Count projects: %d%n", client.count());

        client.save(project);
        client.clear(projects);
        System.out.printf("Count projects: %d%n", client.count());

        client.save(project);
        System.out.printf("Count projects: %d%n", client.count());
        client.clear();
        System.out.printf("Count projects: %d%n", client.count());
    }

}
