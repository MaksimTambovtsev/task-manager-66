package ru.tsc.tambovtsev.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.tsc.tambovtsev.tm.api.endpoint.TaskRestEndpoint;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Arrays;
import java.util.List;

public class TaskRestEndpointClient implements TaskRestEndpoint {

    @NotNull
    private static final String ROOT_URL = "http://localhost:8080/api/task/";

    @Override
    public List<Task> findAll() {
        @NotNull final String localUrl = "findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        return Arrays.asList(template.getForObject(ROOT_URL + localUrl, Task[].class));
    }

    @Override
    public Task findById(@NotNull final String id) {
        @NotNull final String localUrl = "findById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Task.class, id);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        @NotNull final String localUrl = "existsById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Boolean.class, id);
    }

    @Override
    public Task save(@NotNull final Task task) {
        @NotNull final String localUrl = "save";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(task, headers);
        return template.postForObject(ROOT_URL + localUrl, entity, Task.class);
    }

    @Override
    public void delete(@NotNull final Task task) {
        @NotNull final String localUrl = "delete";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(task, headers);
        template.postForObject(ROOT_URL + localUrl, entity, Task.class);
    }

    @Override
    public void clear(@NotNull final List<Task> task) {
        @NotNull final String localUrl = "deleteAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(task, headers);
        template.postForObject(ROOT_URL + localUrl, entity, Task.class);
    }

    @Override
    public void clear() {
        @NotNull final String localUrl = "clear";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(ROOT_URL + localUrl);
    }

    @Override
    public void deleteById(@NotNull final String id) {
        @NotNull final String localUrl = "deleteById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(ROOT_URL + localUrl, id);
    }

    @Override
    public long count() {
        @NotNull final String localUrl = "count";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Long.class);
    }

    public static void main(String[] args) {
        TaskRestEndpointClient client = new TaskRestEndpointClient();

        @NotNull final List<Task> tasks = client.findAll();
        System.out.println("List tasks:");
        for(@NotNull final Task task: tasks) {
            System.out.printf("Task: %s; ID: %s%n", task.getName(), task.getId());
        }
        System.out.println();

        @NotNull final String id = tasks.get(0).getId();

        System.out.printf("Count tasks: %d%n", client.count());
        System.out.printf("Exists task: %b%n", client.existsById(id));

        @Nullable final Task task = client.findById(id);
        System.out.printf("Find task: %s%n", task.getName());

        client.deleteById(id);
        System.out.printf("Count tasks: %d%n", client.count());
        System.out.printf("Exists task: %b%n", client.existsById(id));

        client.save(task);
        System.out.printf("Count tasks: %d%n", client.count());

        client.delete(task);
        System.out.printf("Count tasks: %d%n", client.count());

        client.save(task);
        client.clear(tasks);
        System.out.printf("Count tasks: %d%n", client.count());

        client.save(task);
        System.out.printf("Count tasks: %d%n", client.count());
        client.clear();
        System.out.printf("Count tasks: %d%n", client.count());
    }

}
