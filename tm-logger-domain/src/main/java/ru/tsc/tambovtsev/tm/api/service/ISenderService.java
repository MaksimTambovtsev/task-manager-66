package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.dto.logger.EntityLogDTO;

public interface ISenderService {

    void send(@NotNull EntityLogDTO entity);

    EntityLogDTO createMessage(@NotNull Object object, @NotNull String type);

}
