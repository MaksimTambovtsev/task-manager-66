package ru.tsc.tambovtsev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public final class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(@Nullable UserDTO user) {
        super(user);
    }
}
